/*
 * ADPluginDroop.cpp
 *
 * Author: Hinko Kocevar
 *
 * Created September 25, 2018
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <epicsTypes.h>
#include <epicsMessageQueue.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsTime.h>
#include <iocsh.h>

#include <asynDriver.h>

#include <epicsExport.h>
#include "NDPluginDriver.h"
#include "ADPluginDroop.h"

static const char *driverName="ADPluginDroop";

template <typename epicsType>
asynStatus ADPluginDroop::doDroopCompensationT(NDArray *pArray)
{
    epicsType *pData = (epicsType *)pArray->pData;
    size_t i;
    size_t nElements;
    NDArrayInfo arrayInfo;

    double droopRatePercent = 0;
    getDoubleParam(mADPluginDroopRate, &droopRatePercent);
    double droopTimePerPoint = 0;
    getDoubleParam(mADPluginDroopTimePerPoint, &droopTimePerPoint);

    pArray->getInfo(&arrayInfo);
    nElements = arrayInfo.nElements;

    double accu = 0;
    double err = 0;
    double elem = 0;
    // XXX: fixme this should not be hardcoded!
    // Hz
    // double fs = 88052500;
    // double fs = 88333333;
    // the droop rate from user is expressed as %/ms, time per point
    // is expressed in seconds
    double droopRate = (droopRatePercent * 1000 / 100) * droopTimePerPoint;
    double temp = (double)(pData[0]);
    for (i = 1; i < nElements; i++) {
        // previous sample value
        elem = temp;
        // droop correction
        err = elem * droopRate;
        // add to accumulator
        accu += err;
        // remeber the current value for next loop
        temp = (double)pData[i];
        // fix output value
        pData[i] = (epicsType)(elem + accu);

        // if ((i % 10000) == 0) {
        //     printf("[%ld] accu %f, err %f, in %f, out %f, rate %f\n",
        //         i, accu, err,
        //         elem,
        //         elem + err,
        //         (err / elem * 100.0));
        // }
    }
    // printf("[END] accu %f, err %f, in %f, out %f, rate %f\n",
    //     accu, err,
    //     elem,
    //     elem + err,
    //     (err / elem * 100.0));

    return asynSuccess;
}

asynStatus ADPluginDroop::doDroopCompensation(NDArray *pArray)
{
    asynStatus status;

    switch(pArray->dataType) {
        case NDInt8:
            status = doDroopCompensationT<epicsInt8>(pArray);
            break;
        case NDUInt8:
            status = doDroopCompensationT<epicsUInt8>(pArray);
            break;
        case NDInt16:
            status = doDroopCompensationT<epicsInt16>(pArray);
            break;
        case NDUInt16:
            status = doDroopCompensationT<epicsUInt16>(pArray);
            break;
        case NDInt32:
            status = doDroopCompensationT<epicsInt32>(pArray);
            break;
        case NDUInt32:
            status = doDroopCompensationT<epicsUInt32>(pArray);
            break;
        case NDFloat32:
            status = doDroopCompensationT<epicsFloat32>(pArray);
            break;
        case NDFloat64:
            status = doDroopCompensationT<epicsFloat64>(pArray);
            break;
        default:
            status = asynError;
        break;
    }
    return status;
}

/** Callback function that is called by the NDArray driver with new NDArray data.
  * \param[in] pArray  The NDArray from the callback.
  */
void ADPluginDroop::processCallbacks(NDArray *pArray)
{
    /* This function calls droop compensation algorithm in the input data.
     * It is called with the mutex already locked.
     */

    NDArrayInfo_t arrayInfo;
    static const char* functionName = "processCallbacks";

    // Call the base class method.
    NDPluginDriver::beginProcessCallbacks(pArray);

    pArray->getInfo(&arrayInfo);

    // Take the data type and dimensions from input pArray.
    // Plugin accepts 1D, or 2D (with yDim = 1) NDArrays.
    if (! ((pArray->ndims == 1) ||
            ((pArray->ndims == 2) && (arrayInfo.yDim == 1)))) {
        asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
            "%s::%s, invalid array dimensions\n",
            driverName, functionName);
        return;
    }

    // Create a copy of original array data that will hold droop compensated data.
    NDArray *pDroopCompArray = this->pNDArrayPool->copy(pArray, NULL, 1);
    // Perform droop compensation on input data.
    asynStatus status = doDroopCompensation(pDroopCompArray);
    if (status != asynSuccess) {
        asynPrint(this->pasynUserSelf, ASYN_TRACE_ERROR,
            "%s::%s, droop compensation failed\n",
            driverName, functionName);
        pDroopCompArray->release();
        return;
    }

    pDroopCompArray->uniqueId  = pArray->uniqueId;
    pDroopCompArray->timeStamp = pArray->timeStamp;
    pDroopCompArray->epicsTS   = pArray->epicsTS;

    // Release the lock.
    // While it is released we cannot access the parameter library or class member data.
    // this->unlock();
    // XXX do stuff here..
    // We must exit with the mutex locked.
    // this->lock();

    // We are passing a copied array and attributes have been transferred by
    // NDArrayPool::copy() to array copy.
    // Let plugin base execute NDArray callbacks to downstream plugins
    // if NDArrayCallbacks is true.
    NDPluginDriver::endProcessCallbacks(pDroopCompArray, false, false);

    // Update the parameters.
    callParamCallbacks();
}

/** Constructor for ADPluginDroop; all parameters are simply passed to NDPluginDriver::NDPluginDriver.
  * This plugin cannot block (ASYN_CANBLOCK=0) and is not multi-device (ASYN_MULTIDEVICE=0).
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] queueSize The number of NDArrays that the input queue for this plugin can hold when
  *            NDPluginDriverBlockingCallbacks=0.  Larger queues can decrease the number of dropped arrays,
  *            at the expense of more NDArray buffers being allocated from the underlying driver's NDArrayPool.
  * \param[in] blockingCallbacks Initial setting for the NDPluginDriverBlockingCallbacks flag.
  *            0=callbacks are queued and executed by the callback thread; 1 callbacks execute in the thread
  *            of the driver doing the callbacks.
  * \param[in] NDArrayPort Name of asyn port driver for initial source of NDArray callbacks.
  * \param[in] NDArrayAddr asyn port driver address for initial source of NDArray callbacks.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *      allowed to allocate. Set this to 0 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to 0 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] maxThreads The maximum number of threads this driver is allowed to use. If 0 then 1 will be used.
  */
ADPluginDroop::ADPluginDroop(const char *portName, int queueSize, int blockingCallbacks,
                                     const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory,
                                     int priority, int stackSize, int maxThreads)
    /* Invoke the base class constructor */
    : NDPluginDriver(portName, queueSize, blockingCallbacks,
                   NDArrayPort, NDArrayAddr, 1, maxBuffers, maxMemory,

                   asynInt8ArrayMask | asynInt16ArrayMask | asynInt32ArrayMask |
                   asynFloat32ArrayMask | asynFloat64ArrayMask,

                   asynInt8ArrayMask | asynInt16ArrayMask | asynInt32ArrayMask |
                   asynFloat32ArrayMask | asynFloat64ArrayMask,

                   /* asynFlags is set to 0, because this plugin cannot block and is not multi-device.
                    * It does autoconnect */
                   0, 1, priority, stackSize, maxThreads)
{
    static const char *functionName = "ADPluginDroop";
    printf("%s::%s() starting..\n", driverName, functionName);

    createParam(ADPluginDroopRate_,             asynParamFloat64, &mADPluginDroopRate);
    createParam(ADPluginDroopTimePerPoint_,     asynParamFloat64, &mADPluginDroopTimePerPoint);

    /* Set the plugin type string */
    setStringParam(NDPluginDriverPluginType, "ADPluginDroop");
    /* Set the plugin data type to NDFloat64; double */
    setIntegerParam(NDDataType, NDFloat64);

    // Enable ArrayCallbacks.
    // This plugin can do array callbacks, so make the setting reflect the behavior
    setIntegerParam(NDArrayCallbacks, 1);

    /* Try to connect to the NDArray port */
    connectToArrayPort();
}

/* Configuration routine.  Called directly, or from the iocsh function */
extern "C" int ADDroopConfigure(const char *portName, int queueSize, int blockingCallbacks,
                                    const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory,
                                    int priority, int stackSize, int maxThreads)
{
    ADPluginDroop *pPlugin = new ADPluginDroop(portName, queueSize, blockingCallbacks, NDArrayPort, NDArrayAddr,
                                                       maxBuffers, maxMemory, priority, stackSize, maxThreads);
    return pPlugin->start();
}


/* EPICS iocsh shell commands */
static const iocshArg initArg0 = { "portName",iocshArgString};
static const iocshArg initArg1 = { "frame queue size",iocshArgInt};
static const iocshArg initArg2 = { "blocking callbacks",iocshArgInt};
static const iocshArg initArg3 = { "NDArrayPort",iocshArgString};
static const iocshArg initArg4 = { "NDArrayAddr",iocshArgInt};
static const iocshArg initArg5 = { "maxBuffers",iocshArgInt};
static const iocshArg initArg6 = { "maxMemory",iocshArgInt};
static const iocshArg initArg7 = { "priority",iocshArgInt};
static const iocshArg initArg8 = { "stack size",iocshArgInt};
static const iocshArg initArg9 = { "# threads",iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6,
                                            &initArg7,
                                            &initArg8,
                                            &initArg9};
static const iocshFuncDef initFuncDef = {"ADDroopConfigure",10,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    ADDroopConfigure(args[0].sval, args[1].ival, args[2].ival,
                            args[3].sval, args[4].ival, args[5].ival,
                            args[6].ival, args[7].ival, args[8].ival,
                            args[9].ival);
}

extern "C" void ADDroopRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

extern "C" {
epicsExportRegistrar(ADDroopRegister);
}
