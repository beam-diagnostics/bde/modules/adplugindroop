#ifndef ADPluginDroop_H
#define ADPluginDroop_H

#include <epicsTypes.h>

#include "NDPluginDriver.h"

#define ADPluginDroopRate_                      "DROOP.RATE"                    /* (asynFloat64, r/w) Droop rate in %/ms */
#define ADPluginDroopTimePerPoint_              "DROOP.TIME_PER_POINT"          /* (asynFloat64, r/w) Time point value in seconds */

/** Applies droop compensation on NDArray callback data. */
class epicsShareClass ADPluginDroop : public NDPluginDriver {
public:
    ADPluginDroop(const char *portName, int queueSize, int blockingCallbacks,
                      const char *NDArrayPort, int NDArrayAddr, int maxBuffers, size_t maxMemory,
                      int priority, int stackSize, int maxThreads=1);

    /* These methods override the virtual methods in the base class */
    void processCallbacks(NDArray *pArray);

	template <typename epicsType> asynStatus doDroopCompensationT(NDArray *pArray);
	asynStatus doDroopCompensation(NDArray *pArray);

protected:
    int mADPluginDroopRate;
    #define FIRST_ADPLUGINDROOP_PARAM mADPluginDroopRate
    int mADPluginDroopTimePerPoint;
private:
};

#endif
