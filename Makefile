#Makefile at top of application tree
TOP = .
include $(TOP)/configure/CONFIG
DIRS := $(DIRS) $(filter-out $(DIRS), configure)
DIRS := $(DIRS) $(filter-out $(DIRS), $(wildcard *App))
# DIRS := $(DIRS) $(filter-out $(DIRS), $(wildcard *AppDemo))
# DIRS := $(DIRS) $(filter-out $(DIRS), $(wildcard iocBoot))

define DIR_template
 $(1)_DEPEND_DIRS = configure
endef
$(foreach dir, $(filter-out configure,$(DIRS)),$(eval $(call DIR_template,$(dir))))

# define DIR_template_demoapp
#  $(1)_DEPEND_DIRS += $(filter %App,$(DIRS))
# endef
# $(foreach dir, $(wildcard *AppDemo),$(eval $(call DIR_template_demoapp,$(dir))))

# iocBoot_DEPEND_DIRS += $(filter %App,$(DIRS))
# iocBoot_DEPEND_DIRS += $(filter %AppDemo,$(DIRS))

include $(TOP)/configure/RULES_TOP
