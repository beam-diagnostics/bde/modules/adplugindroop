< envPaths
errlogInit(20000)

dbLoadDatabase("$(TOP)/dbd/ADPluginDroopAppDemo.dbd")
ADPluginDroopAppDemo_registerRecordDeviceDriver(pdbbase)

epicsEnvSet("PREFIX", "$(PREFIX=13SIM1:)")
epicsEnvSet("SIMDET_PORT", "SIM1")
epicsEnvSet("DROOP_PORT", "DROOP")
epicsEnvSet("XSIZE", "$(XSIZE=200)")
epicsEnvSet("YSIZE", "$(YSIZE=1)")
epicsEnvSet("QSIZE", "20")
epicsEnvSet("EPICS_DB_INCLUDE_PATH", "$(ADCORE)/db:$(ADSIMDETECTOR)/db:$(ADPLUGINDROOP)/db")

simDetectorConfig("$(SIMDET_PORT)", $(XSIZE), $(YSIZE), 1, 0, 0)
dbLoadRecords("simDetector.template", "P=$(PREFIX), R=SIM:, PORT=$(SIMDET_PORT), ADDR=0, TIMEOUT=1")

ADDroopConfigure("$(DROOP_PORT)", 3, 1, "$(SIMDET_PORT)")
dbLoadRecords("ADPluginDroop.template", "P=$(PREFIX),R=DRP:,PORT=$(DROOP_PORT),ADDR=0,TIMEOUT=1,NDARRAY_PORT=$(SIMDET_PORT)")

iocInit()

dbpf $(PREFIX)DRP:EnableCallbacks Enable
dbpf $(PREFIX)SIM:AcquirePeriod 2
dbpf $(PREFIX)SIM:Acquire 1

#Always end with a new line
