# Droop compensation

Reference implementation is from Matthias Werner @ DESY, dated 2013-10-13.
VHDL was converted to (pseudo) C by Kaj.
After examining the code, having it simulated in modelsim and testing the
implementation in python following can be said:

* Code assumes 88 MSps sampling rate. Other (higher) rates
might not cause issues; at 125 MSps the number of bits available for
integrator should be enough.

* Code assumes 2.86 ms pulse length. Other pulse lengths might still be
acceptable; for example LEBT pulse of 6 ms.

* Above being said, if not using assumed sampling rate and pulse length,
the droop rate input register value will not relate to the same droop
rate in %. This would result that at higher sampling frequency we would
not compensate for the same droop rate as for 88 MSps. User would need
to adjust the droop rate in % (droop rate register value) to match
new frequency.

## Droop rates

User can enter droop rates from 0 to 12 %.

	Droop [%] = (corr value - adc value) / adc value * 100

Following table lists some input and corrected values for
the selected droop rate in %.

|droop reg|adc value|corr value|Droop [%]|
|---------|---------|----------|---------|
0|30000|30000|0.00%
5000|30000|30274|0.91%
10000|30000|30549|1.83%
15000|30000|30823|2.74%
20000|30000|31098|3.66%
23000|30000|31263|4.21%
25000|30000|31373|4.58%
30000|30000|31647|5.49%
35000|30000|31922|6.41%
40000|30000|32197|7.32%
45000|30000|32472|8.24%
50000|30000|32746|9.15%
55000|30000|33021|10.07%
60000|30000|33296|10.99%
65000|30000|33570|11.90%
65535|30000|33600|12.00%


## VHDL

Original VHDL implementation was simulated in modelsim. For that
purpose simple test bench was created. See `tests/` folder.

Start modelsim compilation or simulation with:

	$ cd tests
	$ bash modelsim vcom mw_droop_compensation_tb.vhd mw_droop_compensation.vhd
	$ bash modelsim vsim mw_droop_compensation_tb.vhd mw_droop_compensation.vhd

Waveform workbench is also saved under `tests/`.

## Python

Python code was used to disect and analyze the VHDL code.
It was also used to prototype C++ code in the plugin.
It can be used to experiment with the implementation.
