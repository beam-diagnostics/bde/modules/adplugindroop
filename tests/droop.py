#!/usr/bin/env python3.5

# droop compensation algorithm
# initial parameters and implementation from MW VHDL code
# mw_droop_compensation.vhd

# Data for plotting
import math
# import matplotlib
import matplotlib.pyplot as plt
import sys


# input ADC value
din = 30000
# users droop rate register value (converted from droop %)
urate = 23000

if len(sys.argv) > 1:
    din = int(sys.argv[1])
if len(sys.argv) > 2:
    urate = int(sys.argv[2])
print('User parameters: data samples %d, droop rate %d' % (din, urate))

# drate is unsigned 16 bit value [0 .. 65536]
# it used to scale the input sample value
# XXX: we need to understand what is the relation beetwen
#      drate and droop in %
# value of 23000 relates to 4.2 % of droop when:
#  * integrating over 2.86 ms
#  * using sampling rate 88 MHz
#  * the accumulator divider is set to 21 bits
# if we change the number of bits used for drate we affect the accumulator
# value hence we have to correct the divider value, too (this was done for
# debugging..)
# here is a mapping of drate bit width --> (drate, accu_div),
# in which we always obtain 4.2 % droop, but our drate is fewer in bits.
drate2div = {
    16: (urate,      21),   # 23000
    15: (urate >> 1, 20),   # 11500
    14: (urate >> 2, 19),   # 5750
    13: (urate >> 3, 18),   # 2875
    12: (urate >> 4, 17)    # 1437
}
# default to 16 bit drate value, 21 bit shift
drate = drate2div[16][0]
accu_div = drate2div[16][1]

# sampling rate in Hz, used to calculate number of samples within a pulse
fsampl = 88e6
# sample delta time in s
# delta = 1 / fsampl
# pulse length in ms
lenms = 2.86
# nr elements within pulse
n = int(lenms * fsampl / 1e3)
print('Will have %d elements in %f ms pulse' % (n, lenms))

v = 0
aiArr = []

# a bit of flat line at the start of the pulse
a = 0
for i in range(0, a):
    aiArr.append(0)
# now the decline
b = int(n)
for i in range(0, b):
    # angle in degrees = atan(angle in percent / 100%)
    # https://rechneronline.de/winkel/percent.php
    # aiArr.append(v*math.tan(math.atan(rate/100.0)))
    aiArr.append(din)
    # v -= 1
# a bit of flat line at the end of the pulse
c = 0
for i in range(0, c):
    aiArr.append(0)

n = a + b + c
print('aiArr has %d elements, val %d' % (len(aiArr), din))

accuArr = []
aoArr = []
errArr = []
dxArr = []
epsArr = []
accu = 0
for i in range(0, n):
    # aiArr[i] is signed 16 bit value [-32768 .. 32767]
    # dx is 32 bit signed value
    dx = abs(drate * aiArr[i])
    # use divide by 2^16 due to multiplication above
    dx = (dx >> 16) & 0xFFFF
    # add to accumulator
    accu += dx
    # over the 2.86 ms @ 88MHz we process ~250 000 samples
    # accu can reach value of 2^17 (sample value) * 2^18 (nr samples value) = 2^35
    # accu_div depends on the drate bit width
    err = (accu >> accu_div) & 0xFFFF
    # correct the output value
    ao = aiArr[i] + err
    if (i % 10000) == 0:
        print('[%d] accu %f, err %f, dx %f, sample %f -> %f' %
              (i, accu, err, dx, aiArr[i], ao))

    # foo = input()
    dxArr.append(dx)
    aoArr.append(ao)
    accuArr.append(accu)
    errArr.append(err)
    epsArr.append(0-ao)

print('[%d] accu %f, err %f, dx %f, sample %f -> %f' %
      (n-1, accu, err, dx, aiArr[n-1], ao))

print('Droop rate: %.2f %%' % ((ao-aiArr[n-1])/din*100.0))


# plot
x = [i for i in range(0, n)]
plt.plot(x, [0 for i in range(0, n)], linewidth=1.0, linestyle='--', label='0')
plt.plot(x, errArr,  linewidth=1.0, linestyle='--', label='err')
# plt.plot(x, accuArr, linewidth=1.0, linestyle='--', label='accu')
# plt.plot(x, errArr,  linewidth=1.0, linestyle='--', label='err')
# plt.plot(x, dxArr,   linewidth=1.0, linestyle='--', label='dx')
# plt.plot(x, epsArr,  linewidth=1.0, linestyle='--', label='eps')
plt.plot(x, aiArr,   linewidth=2.0, linestyle='-',  label='ai')
plt.plot(x, aoArr,   linewidth=2.0, linestyle='-',  label='ao')

plt.xlabel('Samples')
plt.ylabel('ADC counts')
plt.title("Droop correction")
plt.legend()
plt.show()
