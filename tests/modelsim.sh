#!/bin/bash

set -x
P="/home/hinxx/ess/fpga/intelFPGA_pro/18.1/modelsim_ase/bin"
if [ -z "$1" ]; then echo "missing arguments: vcom|vsim [<files>]"; exit 1; fi
cmd="$1"
shift
exec ${P}/${cmd} ${@}
set +x
