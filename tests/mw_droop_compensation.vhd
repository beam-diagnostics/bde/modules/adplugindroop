----------------------------------------------------------------------------------
-- Company:        DESY
-- Engineer:       Matthias Werner
-- Create Date:    2013-10-13
-- Module Name:    droop_compens 
----------------------------------------------------------------------------------
-- Description:

-- Droop compensation for an ACCT beam current monitor

-- Example with drooprate = 4.2% after 2.86 ms,  clk = 88 MHz:
--		s17_din (ADC value): 3.00e4
--		u16_drooprate_in:    2.30e4
--		--> s34_din_mul:		6.90e8
--		--> s17_din_mul_div:	1.05e4
--		--> s39_akku after integration over 2.86 ms @ 88 MHz: 2.65e9 -> this is 4.2% of s17_din
--		--> s17_dout will be s17_din + 4.2% -> correct compensation at clk = 88 MHz

-- Resources:
--		55 REGs, 56 LUTs, 1 DSP48E
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity droop_compens is
    Port ( clk              : in  STD_LOGIC;
           reset_in         : in  STD_LOGIC;         -- Disable droop compensation outside beam window
           u16_drooprate_in : in  unsigned (15 downto 0);
           s17_din          : in  signed (16 downto 0);
           s17_dout         : out  signed (16 downto 0) := (others => '0'));
end droop_compens;

architecture Behavioral of droop_compens is

	signal s39_akku			: signed (38 downto 0) := (others => '0');	-- For integration of ADC values
	signal s34_din_mul		: signed (33 downto 0) := (others => '0');
	signal s17_din_mul_div	: signed (16 downto 0) := (others => '0');

begin

	-- act on clk and reset_in signals
	proc_droop : process(clk, reset_in)
	begin
		if rising_edge(clk) then
			-- d_in * drooprate; drooprate ist converted to signed by adding a '0'
			s34_din_mul <= s17_din * signed('0' & u16_drooprate_in);
			-- / 2^16; the MSB is cut off because it equals MSB-1 due to the '0' added above
			-- take 17 bits!! without the MSB(33), start at MSB-1(32)
			s17_din_mul_div <= s34_din_mul(32 downto 16);
			if reset_in = '1' then
				s39_akku <= (others => '0');
			else
				-- Accumulate: over 2.86 ms:
				-- 88 MHz * 2.86 ms = 2.52e5 samples
				-- 18 bits needed to represent 2.52e5 samples
				s39_akku <= s39_akku + s17_din_mul_div;
			end if;
			-- d_out: d_in + accu / 2^21
			-- take 17 bits, without the MSB(38), start at MSB-1(37)
			s17_dout <= s17_din + s39_akku(37 downto 21);
		end if;
	end process;

end Behavioral;
