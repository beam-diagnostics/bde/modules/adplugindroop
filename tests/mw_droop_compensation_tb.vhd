----------------------------------------------------------------------------------
-- Company:        ESS
-- Engineer:       Hinko Kocevar
-- Create Date:    2018-10-09
-- Module Name:    droop_compens test bench
----------------------------------------------------------------------------------
-- Description:

-- From droop compensation module:

-- Example with drooprate = 4.2% after 2.86 ms,  clk = 88 MHz:
--    s17_din (ADC value): 3.00e4
--    u16_drooprate_in:    2.30e4
--    --> s34_din_mul:    6.90e8
--    --> s17_din_mul_div:  1.05e4
--    --> s39_akku after integration over 2.86 ms @ 88 MHz: 2.65e9 -> this is 4.2% of s17_din
--    --> s17_dout will be s17_din + 4.2% -> correct compensation at clk = 88 MHz

----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

entity droop_compens_tb is
end droop_compens_tb;
 
architecture behave of droop_compens_tb is
  signal r_clk              : std_logic := '0';
  -- start in reset
  signal r_reset_in         : std_logic := '1';
  signal r_u16_drooprate_in : unsigned (15 downto 0);
  signal r_s17_din          : signed (16 downto 0);
  -- set output to 0 while not enabled
  signal w_s17_dout         : signed (16 downto 0) := (others => '0');

  -- debug..
  signal r_s17_test         : signed (16 downto 0);
  signal r_s34_test2        : signed (33 downto 0);
  signal r_s39_test3        : signed (38 downto 0);

  constant c_CLK_PERIOD : time := 11.36363636 ns;

  component droop_compens is
    port (
     clk              : in  STD_LOGIC;
     reset_in         : in  STD_LOGIC;         -- Disable droop compensation outside beam window
     u16_drooprate_in : in  unsigned (15 downto 0);
     s17_din          : in  signed (16 downto 0);
     s17_dout         : out signed (16 downto 0) := (others => '0')
     );
  end component droop_compens;
begin
   
  droop_INST : droop_compens
    port map (
      clk               => r_clk,
      reset_in          => r_reset_in,
      u16_drooprate_in  => r_u16_drooprate_in,
      s17_din           => r_s17_din,
      s17_dout          => w_s17_dout
      );
 

  -- generate clock
  clk_gen : process is
  begin
    r_clk <= '0' after c_CLK_PERIOD/2, '1' after c_CLK_PERIOD;
    wait for c_CLK_PERIOD;
  end process clk_gen;

  init_once : process is
  begin
    -- debug..

    -- these will assign 17 bits
    r_s17_test <= r_s34_test2(16 downto 0);
    -- but do not take MSB, but 17 bits from MSB-1 ..
    r_s17_test <= r_s34_test2(32 downto 16);
    r_s17_test <= r_s39_test3(37 downto 21);
    -- debug..

    r_u16_drooprate_in <= "0101100111011000"; -- X"59D8";  -- 23000
    r_s17_din          <= "00111010100110000"; -- X"7530";  -- 30000

    wait for 20 ns;
    -- out of reset
    r_reset_in         <= '0';

    -- wait forever
    wait;
  end process init_once;
  
  process
  begin

    wait;

  end process;
     
end behave;
